// Insert Single Room
db.hotelRooms.insertOne(
	{
		name: "single",
		accomodates: 2,
		price: 1000,
		description: "A simple room with all the basic necessities",
		rooms_available: 10,
		isAvailable: false
	}
);

// Insert Multiple Room
db.hotelRooms.insertMany([
	{
		name: "double",
		accomodates: 3,
		price: 2000,
		description: "A room fit for a small family going on a vacation",
		rooms_available: 5,
		isAvailable: false
	},
	{
		name: "queen",
		accomodates: 4,
		price: 4000,
		description: "A room with a queen size bed perfect for a simple getaway",
		rooms_available: 15,
		isAvailable: false
	}
]);

// Search a room with the name 'double'
db.hotelRooms.find(
	{
		name: "double"
	}
);

// Update queen room to 0 available rooms
db.hotelRooms.updateOne(
	{
		name: "queen"
	},
	{
		$set: {
			rooms_available: 0,
		}
	}
);

// Delete all rooms that have 0 availability
db.hotelRooms.deleteMany(
	{
		rooms_available: 0,
	}
);